package com.classpath.orders.dto;

import java.time.LocalDate;

public interface OrderDto {
	
	String getCustomerName();
	
	String getCustomerEmail();
	
	double getPrice();
	
	LocalDate getOrderDate();
}
