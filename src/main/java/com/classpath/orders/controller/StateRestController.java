package com.classpath.orders.controller;

import static org.springframework.boot.availability.LivenessState.BROKEN;
import static org.springframework.boot.availability.LivenessState.CORRECT;
import static org.springframework.boot.availability.ReadinessState.ACCEPTING_TRAFFIC;
import static org.springframework.boot.availability.ReadinessState.REFUSING_TRAFFIC;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.boot.availability.AvailabilityChangeEvent.publish;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/state")
@RequiredArgsConstructor
@Slf4j
public class StateRestController {

	private final ApplicationAvailability applicationAvailibility;
	private final ApplicationEventPublisher applicationEvent;

	@PostMapping("/liveness")
	public Map<String, Object> liveness() {
		LivenessState livenessState = this.applicationAvailibility.getLivenessState();
		log.info("LivenessState " + livenessState);
		LivenessState updatedLivenessState = livenessState == CORRECT ? BROKEN : CORRECT;
		String state = updatedLivenessState == CORRECT ? "System is functioning" : "Application is not functioning";

		Map<String, Object> responseMap = new LinkedHashMap<>();

		// publish the change event
		publish(applicationEvent, state, updatedLivenessState);

		responseMap.put("liveness", updatedLivenessState);
		responseMap.put("state", state);
		return responseMap;
	}

	@PostMapping("/readiness")
	public Map<String, Object> readiness() {
		ReadinessState readinessState = this.applicationAvailibility.getReadinessState();
		ReadinessState updatedReadinessState = readinessState == ACCEPTING_TRAFFIC ? REFUSING_TRAFFIC
				: ACCEPTING_TRAFFIC;

		String state = updatedReadinessState == ACCEPTING_TRAFFIC ? "System is functioning"
				: "Application is not functioning";

		// publish the change event
		publish(applicationEvent, state, updatedReadinessState);

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("readiness", updatedReadinessState);
		responseMap.put("state", state);
		return responseMap;

	}

}
