package com.classpath.orders.util;

import static java.util.stream.IntStream.range;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.classpath.orders.model.LineItem;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
//@Profile({"dev","qa"})
public class BootstrapAppData { // implements ApplicationListener<ApplicationReadyEvent>{
	
	private final OrderRepository orderRepository;
	private final Faker faker = new Faker();
	
	@Value("${app.orderCount}")
	private int counter;

	//@Override
	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationEvent(ApplicationReadyEvent event) {
	
		range(0, counter).forEach(index -> {
			String firstName = faker.name().firstName();
			//Order order = new Order("Harish", "Vinay", 22, 34, true, true, null);			
			Order order = Order.builder()
								.customerEmail(firstName + "@"+ faker.internet().domainName())
								.customerName(firstName)
								.orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.build();
			
			IntStream.range(0, faker.number().numberBetween(2, 4)).forEach(var -> {
				LineItem lineItem = LineItem.builder()
										.name(faker.commerce().productName())
										.qty(faker.number().numberBetween(2, 5))
										.pricePerUnit(faker.number().randomDouble(2, 400, 600))
										.build();
				order.addLineItem(lineItem);
			});
			double orderValue = order.getLineItems().stream()
										.map(lineItem -> lineItem.getQty() * lineItem.getPricePerUnit())
										.reduce(0d, Double::sum);
			order.setPrice(orderValue);
			this.orderRepository.save(order);
		});
	}
	
	

}
