package com.classpath.orders.util;

import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ApplicationStateEventHandler {
	@EventListener(AvailabilityChangeEvent.class)
	public void subscribeApplicationEvent(AvailabilityChangeEvent event) {
		log.info("Avaialabiilty change event :: "+event.getState());
		log.info("Avaialabiilty change event :: "+event.getPayload());
		log.info("Avaialabiilty change event :: "+event.getSource());
	}
}
