package com.classpath.orders.event;

import java.time.LocalDateTime;
import com.classpath.orders.model.Order;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class OrderEvent {
	
	private final Event event;
	private final Order payload;
	private LocalDateTime timestamp = LocalDateTime.now();
	

}
