package com.classpath.orders.event;

public enum Event {
	
	ORDER_ACCEPTED,
	ORDER_PENDING,
	ORDER_FULFILLED,
	ORDER_CANCELLED

}
