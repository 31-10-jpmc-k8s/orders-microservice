package com.classpath.orders.model;

import static lombok.AccessLevel.PRIVATE;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor
@Builder

@Entity
@Table(name="orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotBlank(message="customer name cannot be blank")
	private String customerName;
	
	@NotBlank(message="customer email cannot be blank")
	@Email(message="email is not in correct format")
	private String customerEmail;
	
	@Min(value=500, message = "minimum order price is 500")
	@Max(value=50000, message = "maximum order price is 5000")
	private double price;
	
	@PastOrPresent(message="order date cannot be in the past")
	private LocalDate orderDate;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonManagedReference
	private Set<LineItem> lineItems;
	
	//scaffolding code
	public void addLineItem(LineItem lineItem) {
		if(this.lineItems == null) {
			this.lineItems = new HashSet<>();
		}
		this.lineItems.add(lineItem);
		lineItem.setOrder(this);
	}

}
