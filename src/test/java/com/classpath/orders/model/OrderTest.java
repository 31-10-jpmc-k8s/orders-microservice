package com.classpath.orders.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//unit testing
public class OrderTest {

	private Order order;

	@BeforeEach
	void setupOrder() {
		order = Order.builder().customerEmail("pradeep@gmail.com").customerName("Pradeep").id(12)
				.orderDate(LocalDate.now()).price(400).build();
	}

	@AfterEach
	void tearDown() {
		order = null;
	}

	@Test
	void testConstructor() {
		/*
		 * 1. Create the object under test 2. Assert the behavior
		 */
		assertNotNull(order, "Order object is  created");
		assertEquals(order.getCustomerEmail(), "pradeep@gmail.com");
		assertEquals("Pradeep", order.getCustomerName());
		assertEquals(LocalDate.now(), order.getOrderDate());
		assertEquals(400, order.getPrice());
	}

	@Test
	void testToString() {
		Order order = Order.builder().customerEmail("pradeep@gmail.com").customerName("Pradeep").id(12)
				.orderDate(LocalDate.now()).price(400).build();
		assertEquals(order.toString(),
				"Order(id=12, customerName=Pradeep, customerEmail=pradeep@gmail.com, price=400.0, orderDate=2022-10-02, lineItems=null)");
	}

}
